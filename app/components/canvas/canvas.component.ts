import { Component, Input } from '@angular/core';
import { CellComponent } from '../cell/cell.component';
import { Cell } from '../../shared/cell.model';
import { Utils } from '../../shared/functions.global';

@Component({
	moduleId: module.id,
	selector: 'canvas-component',
	templateUrl: 'canvas.component.html',
	styleUrls: ['canvas.component.css'],
	directives: [CellComponent]
})
export class CanvasComponent {
	@Input() mines: Cell[][];

	onExpandCells(cell: Cell) {
		let mines = this.mines;
		let cellX = cell.index[0];
		let cellY = cell.index[1];

		robot( cellX, cellY );

		function robot( x: number, y: number ) {
			let grid = Utils.gridCounter( mines, x, y );
			grid.forEach((index) => {
				let x = index[0];
				let y = index[1];
				if (mines[x][y].type === 'empty' && !mines[x][y].opened && !mines[x][y].flaged) {
					mines[x][y].opened = true;
					robot(x, y);
				}

				if(mines[x][y].type === 'counter') {
					mines[x][y].opened = true;
				}
			});
		}
	}
}