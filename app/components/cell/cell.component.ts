import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Cell } from '../../shared/cell.model';
import { Utils } from '../../shared/functions.global';

@Component({
	moduleId: module.id,
	selector: 'cell-component',
	templateUrl: 'cell.component.html',
	styleUrls: ['cell.component.css']
})

export class CellComponent {
	@Input() cell: Cell;
	@Input() mines: Cell[][];
	@Output() expanded = new EventEmitter();

	detonate() {
		this.cell.detonated = true;
		Utils.scamperArray(this.mines, (item: Cell) => {
			(!item.flaged) ? 
				item.opened = true : 
				item.detonated = item.type !== 'mine';
		});
	}

	open() {
		this.cell.opened = true;
	}

	expand() {
		this.expanded.emit(this.cell);
	}

	clickedRight(cell: Cell) {
		console.log('right click');
		this.cell.flaged = !this.cell.flaged;
		return false;
	}

	clicked(cell: Cell) {
		switch (cell.type) {
			case 'mine': this.detonate(); break;
			case 'empty': this.expand(); break;
			case 'counter': this.open(); break;
		}
	}
}