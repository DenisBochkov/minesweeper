import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Settings } from '../../shared/settings.model';

@Component({
	moduleId: module.id,
	selector: 'settings-component',
	templateUrl: 'settings.component.html',
	styleUrls: ['settings.component.css']
})

export class SettingsComponent {
	@Input() settings: Settings;
	@Output() changedState = new EventEmitter();
	@Output() changedSettings = new EventEmitter();

	toggleSettings(): void {
		this.changedState.emit();
	}

	changeSettings(x: string, y: string, count: string): void {
		this.settings.countMines = count;
		this.settings.sizeCanvas = {
			y: y,
			x: x
		}
		this.settings.show = false;
		this.changedSettings.emit();
	}
}