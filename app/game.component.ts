import { Component, Input, EventEmitter } from '@angular/core';
import { CanvasComponent } from './components/canvas/canvas.component';
import { CellComponent } from './components/cell/cell.component';
import { SettingsComponent } from './components/settings/settings.component';
import { Settings } from './shared/settings.model';
import { Cell } from './shared/cell.model';
import { Utils } from './shared/functions.global';

@Component({
	moduleId: module.id,
  selector: 'my-game',
  templateUrl: 'game.component.html',
  styleUrls: ['game.component.css'],
	directives: [CanvasComponent, SettingsComponent, CellComponent]
})
export class GameComponent {
	@Input() mines: Cell[][] = [];
	@Input() settings: Settings;

	constructor() {
		this.settings = new Settings(10, 8, 30);
		this.initGame();
	}

	initGame(): void {
		this.initArray();
		this.initMines();
		this.initCounterMines();
	}

	initArray(): void {
		this.mines = [];
		const mines = this.mines;
		const x = this.settings.sizeCanvas.x;
		const y = this.settings.sizeCanvas.y;

		Utils.scamperArray(mines, (item) => item = new Cell('empty'), x, y);
	}

	initMines(): void {
		let mines = this.mines;
		let sizeX = this.settings.sizeCanvas.x - 1;
		let sizeY = this.settings.sizeCanvas.y - 1;
		let prev = [];

		for (let i = 0; i < this.settings.countMines; i++) {
			let x = Utils.getRandom(0, sizeX);
			let y = Utils.getRandom(0, sizeY);
			let xy = x + '' + y;

			while (prev.includes(xy)) {
				x = Utils.getRandom(0, sizeX);
				y = Utils.getRandom(0, sizeY);
				xy = x + '' + y;
			}

			prev.push(xy);
			mines[x][y].type = 'mine';
		}
	}

	initCounterMines(): void {
		let mines = this.mines;

		for (let i = 0; i < this.settings.sizeCanvas.x; i++) {
			for (let j = 0; j < this.settings.sizeCanvas.y; j++) {
				let current = mines[i][j];
				let counter = 0;
				let arrFilter = Utils.gridCounter( mines, i, j );

				arrFilter.forEach((indexes) => {
					let x = indexes[0];
					let y = indexes[1];
					if (mines[x][y].type === 'mine') counter++;
				});
				if (current.type !== 'mine' && counter) {
					current.value = counter;
					current.type = 'counter';
				}
			}
		}
	}

	changeStateSettings(): void {
		this.settings.show = !this.settings.show;
	}

	onChangeSettings(): void {
		this.initGame();
	}
}