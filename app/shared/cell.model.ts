export interface ICell {
	value: number;
	type: string;
	opened: boolean;
	detonated: boolean;
	flaged: boolean;
	index: number[];
}

export class Cell implements ICell {
	value: number;
	type: string;
	opened: boolean;
	detonated: boolean;
	flaged: boolean;
	index: number[];
	constructor(type: string, index: number[] = [5, 2]) {
		this.index = index;
		this.value = null;
		this.type = type;
		this.detonated = false;
		this.flaged = false;
		this.opened = false; // видимость для веб-морды
	}
}