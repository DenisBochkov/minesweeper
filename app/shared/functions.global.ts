export class Utils {

	static scamperArray(arr, event, ...values) {
		let notInit = !values.length;
		let sizeX = values[0] || arr.length;

		for (let i = 0; i < sizeX; i++) {
			let sizeY = values[1] || arr[i].length;

			if(typeof arr[i] === 'undefined') arr[i] = [];
			for (let j: number = 0; j < sizeY; j++) {
				if(notInit) {
					event(arr[i][j]);
				} else {
					arr[i][j] = event(arr[i][j])
					arr[i][j].index = [i, j];
				};
			}
		}
	}

	static gridCounter(mines: Cell[][], i: number, j: number) {
		let sizeX = mines.length;
		let sizeY = mines[0].length;
		let grid = [
			[i - 1, j - 1],
			[i - 1, j],
			[i - 1, j + 1],
			[i, j - 1],
			[i, j + 1],
			[i + 1, j - 1],
			[i + 1, j],
			[i + 1, j + 1]
		];
		let inRange = (item, x, y) => item >= x && item < y;
		
		return grid.filter((item) => inRange(item[0], 0, sizeX) && inRange(item[1], 0, sizeY));
	}

	static getRandom(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

}
