export class Settings {
	show: boolean;
	countMines: number;
	sizeCanvas: any = {};
	constructor(sizeX: number, sizeY: number, count: number) {
		this.show = false;
		this.countMines = count;
		this.sizeCanvas = {
			y: sizeX,
			x: sizeY
		};
	}
}